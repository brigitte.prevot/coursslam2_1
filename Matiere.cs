﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cours1
{
    class Matiere
    {
        private string lIntitule;
        private string lEnseignant;
        private int leCoefficient;
        private int laDuree;

        public Matiere()
        {
            setIntitule("");
            setEnseignant("");
            setCoefficient(0);
            setDuree(0);
        }

        public Matiere(string unIntitule, string unEnseignant, int unCoefficient, int uneDuree)
        {
            setIntitule(unIntitule);
            setEnseignant(unEnseignant);
            setCoefficient(unCoefficient);
            setDuree(uneDuree);
        }

        public void setIntitule(string unIntitule)
        {
            lIntitule = unIntitule;
        }

        public void setEnseignant(string unEnseignant)
        {
            lEnseignant = unEnseignant;
        }

        public void setCoefficient(int unCoefficient)
        {
            leCoefficient = unCoefficient;
        }

        public void setDuree(int uneDuree)
        {
            laDuree = uneDuree;
        }

        public string getIntitule()
        {
            return lIntitule;
        }

        public string getEnseignant()
        {
            return lEnseignant;
        }

        public int getCoefficient()
        {
            return leCoefficient;
        }

        public int getDuree()
        {
            return laDuree;
        }
    }
}
