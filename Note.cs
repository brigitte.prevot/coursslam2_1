﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cours1
{
    class Note
    {
        /// <summary>
        /// Mémorise l'intitulé de la note
        /// </summary>
        private string lIntitule;
        /// <summary>
        /// Mémorise la valeur de la note
        /// </summary>
        private double laValeur;
        /// <summary>
        /// Mémorise la matière associée à la note
        /// </summary>
        private Matiere laMatiere;
        /// <summary>
        /// Mémorise le coefficient de la note
        /// </summary>
        private int leCoefficient;

        /// <summary>
        /// Construit l'objet en initialisant ses propriétés
        /// </summary>
        public Note()
        {
            setIntitule("");
            setValeur(-1);
            setMatiere(new Matiere());
            setCoefficient(-1);
        }

        /// <summary>
        /// Construit l'objet en affectant à ses propriétés les valeurs passés en paramètres
        /// </summary>
        /// <param name="unIntitule"></param>
        /// <param name="uneValeur"></param>
        /// <param name="uneMatiere"></param>
        /// <param name="unCoefficient"></param>
        public Note(string unIntitule, double uneValeur, Matiere uneMatiere, int unCoefficient)
        {
            setIntitule(unIntitule);
            setValeur(uneValeur);
            setMatiere(uneMatiere);
            setCoefficient(unCoefficient);
        }

        /// <summary>
        /// Affecte à la propriété lIntitule la valeur mémorisée dans le paramètre unIntitule
        /// </summary>
        /// <param name="unIntitule"></param>
        public void setIntitule(string unIntitule)
        {
            lIntitule = unIntitule;
        }

        /// <summary>
        /// Affecte à la propriété laValeur la valeur mémorisée dans le paramètre uneValeur si elle est comprise entre 0 et 20
        /// </summary>
        /// <param name="uneValeur"></param>
        public void setValeur(double uneValeur)
        {
            if(uneValeur < 0 || uneValeur > 20)
            {
                laValeur = -1;
            }
            else
            {
                laValeur = uneValeur;
            }
        }

        /// <summary>
        /// Affecte à la propriété leCoefficient la valeur mémorisée dans le paramètre unCoefficient si la valeur est positive
        /// </summary>
        /// <param name="unCoefficient"></param>
        public void setCoefficient(int unCoefficient)
        {
            if (unCoefficient < 0)
            {
                leCoefficient = -1;
            }
            else
            {
                leCoefficient = unCoefficient;
            }
        }

        /// <summary>
        /// Affecte à la propriété laMatière la valeur mémorisée dans le paramètre uneMatière
        /// </summary>
        /// <param name="uneMatiere"></param>
        public void setMatiere(Matiere uneMatiere)
        {
            laMatiere = uneMatiere;
        }

        /// <summary>
        /// Fournit l'intitulé de la note
        /// </summary>
        /// <returns></returns>
        public string getIntitule()
        {
            return lIntitule;
        }

        /// <summary>
        /// Fournit la valeur de la note
        /// </summary>
        /// <returns></returns>
        public double getValeur()
        {
            return laValeur;
        }

        /// <summary>
        /// Fournit la matière associée à la note
        /// </summary>
        /// <returns></returns>
        public Matiere getMatiere()
        {
            return laMatiere;
        }

        /// <summary>
        /// fournit le coefficient de la note
        /// </summary>
        /// <returns></returns>
        public int getCoefficient()
        {
            return leCoefficient;
        }
    }
}
