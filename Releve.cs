﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cours1
{
    class Releve
    {
        private List<Note> lesNotes;

        public Releve()
        {
            lesNotes = new List<Note>();
        }

        public Releve(List<Note> desNotes)
        {
            lesNotes = desNotes;
        }

        public List<Note> getNotes()
        {
            return lesNotes;
        }

        public ListBox getNotes(Matiere uneMatiere, ListBox uneListe)
        {
            ListBox vNotes = uneListe;
            foreach (Note vNote in lesNotes)
            {
                if (vNote.getMatiere() == uneMatiere)
                {
                   vNotes.Items.Add(vNote.getValeur() + " : " + vNote.getCoefficient());
                }
            }
            return vNotes;
        }

        public double moyenneBruteMatiere(Matiere uneMatiere)
        {
            double vMoyenne = 0;
            int vNbr = 0;
            foreach (Note vNote in lesNotes)
            {
                if (vNote.getMatiere() == uneMatiere)
                {
                    vMoyenne = vMoyenne + vNote.getValeur();
                    vNbr = vNbr + 1;
                }
            }
            vMoyenne = vMoyenne / vNbr;
            return vMoyenne;
        }
    }
}
