﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cours1
{
    class Variables
    {
        private static List<Matiere> lesMatieres = new List<Matiere>();
        private static List<Note> lesNotes = new List<Note>();

        public static List<Matiere> getMatieres()
        {
            return lesMatieres;
        }

        public static List<Note> getNotes()
        {
            return lesNotes;
        }
    }
}
