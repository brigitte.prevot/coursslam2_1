﻿namespace Cours1
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaisieMatieres = new System.Windows.Forms.Button();
            this.btnNotes = new System.Windows.Forms.Button();
            this.btnMoyenneBruteMatiere = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSaisieMatieres
            // 
            this.btnSaisieMatieres.Location = new System.Drawing.Point(47, 23);
            this.btnSaisieMatieres.Name = "btnSaisieMatieres";
            this.btnSaisieMatieres.Size = new System.Drawing.Size(169, 95);
            this.btnSaisieMatieres.TabIndex = 0;
            this.btnSaisieMatieres.Text = "Saisie Matières";
            this.btnSaisieMatieres.UseVisualStyleBackColor = true;
            this.btnSaisieMatieres.Click += new System.EventHandler(this.btnSaisieMatieres_Click);
            // 
            // btnNotes
            // 
            this.btnNotes.Location = new System.Drawing.Point(288, 23);
            this.btnNotes.Name = "btnNotes";
            this.btnNotes.Size = new System.Drawing.Size(169, 95);
            this.btnNotes.TabIndex = 1;
            this.btnNotes.Text = "Saisie Notes";
            this.btnNotes.UseVisualStyleBackColor = true;
            this.btnNotes.Click += new System.EventHandler(this.btnNotes_Click);
            // 
            // btnMoyenneBruteMatiere
            // 
            this.btnMoyenneBruteMatiere.Location = new System.Drawing.Point(47, 170);
            this.btnMoyenneBruteMatiere.Name = "btnMoyenneBruteMatiere";
            this.btnMoyenneBruteMatiere.Size = new System.Drawing.Size(169, 95);
            this.btnMoyenneBruteMatiere.TabIndex = 2;
            this.btnMoyenneBruteMatiere.Text = "Calcul moyenne brute par matière";
            this.btnMoyenneBruteMatiere.UseVisualStyleBackColor = true;
            this.btnMoyenneBruteMatiere.Click += new System.EventHandler(this.btnMoyenneBruteMatiere_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnMoyenneBruteMatiere);
            this.Controls.Add(this.btnNotes);
            this.Controls.Add(this.btnSaisieMatieres);
            this.Name = "frmMenu";
            this.Text = "frmMenu";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSaisieMatieres;
        private System.Windows.Forms.Button btnNotes;
        private System.Windows.Forms.Button btnMoyenneBruteMatiere;
    }
}