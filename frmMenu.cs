﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cours1
{
    public partial class frmMenu : Form
    {
        private frmSaisieMatieres vFeuilleSaisieMatieres;
        private frmSaisieNotes vFeuilleSaisieNotes;
        private frmMoyennesParMatiere vFeuilleMoyennesMatiere;

        public frmMenu()
        {
            InitializeComponent();
        }

        private void btnSaisieMatieres_Click(object sender, EventArgs e)
        {
            vFeuilleSaisieMatieres = new frmSaisieMatieres();
            vFeuilleSaisieMatieres.Show();
        }

        private void btnNotes_Click(object sender, EventArgs e)
        {
            vFeuilleSaisieNotes = new frmSaisieNotes();
            vFeuilleSaisieNotes.Show();
        }

        private void btnMoyenneBruteMatiere_Click(object sender, EventArgs e)
        {
            vFeuilleMoyennesMatiere = new frmMoyennesParMatiere();
            vFeuilleMoyennesMatiere.Show();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
            remplirMatieres(Variables.getMatieres());
            remplirNotes(Variables.getNotes());
        }

        private void remplirMatieres(List<Matiere> desMatieres)
        {
            desMatieres.Add(new Matiere("SI1", "P. SCHNEIDER", 3, 4));
            desMatieres.Add(new Matiere("SI2", "C. AUDRAS", 3, 4));
            desMatieres.Add(new Matiere("SI3", "D. RIEHL", 3, 4));
            desMatieres.Add(new Matiere("SI4", "B. PREVOT", 3, 4));
            desMatieres.Add(new Matiere("EMJ", "I. MERCIER", 2, 6));
        }

        private void remplirNotes(List<Note> desNotes)
        {
            desNotes.Add(new Note("DS1", 15.5, Variables.getMatieres().ElementAt(3), 2));
            desNotes.Add(new Note("TP1",  18, Variables.getMatieres().ElementAt(3), 1));
            desNotes.Add(new Note("DS1",  12.25, Variables.getMatieres().ElementAt(2), 2));
            desNotes.Add(new Note("DS2",  10, Variables.getMatieres().ElementAt(3), 2));
            desNotes.Add(new Note("TP2", 13.5, Variables.getMatieres().ElementAt(3), 1));
        }
    }
}
