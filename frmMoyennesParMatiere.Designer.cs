﻿namespace Cours1
{
    partial class frmMoyennesParMatiere
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstMatieres = new System.Windows.Forms.ComboBox();
            this.lblMatiere = new System.Windows.Forms.Label();
            this.lblMoyenneBrute = new System.Windows.Forms.Label();
            this.txtMoyenneBrute = new System.Windows.Forms.TextBox();
            this.lstNotes = new System.Windows.Forms.ListBox();
            this.lblNotes = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.txtCommentaire = new System.Windows.Forms.TextBox();
            this.lblCommentaire = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lstMatieres
            // 
            this.lstMatieres.FormattingEnabled = true;
            this.lstMatieres.Location = new System.Drawing.Point(137, 53);
            this.lstMatieres.Name = "lstMatieres";
            this.lstMatieres.Size = new System.Drawing.Size(100, 21);
            this.lstMatieres.TabIndex = 26;
            this.lstMatieres.SelectedIndexChanged += new System.EventHandler(this.lstMatieres_SelectedIndexChanged);
            // 
            // lblMatiere
            // 
            this.lblMatiere.AutoSize = true;
            this.lblMatiere.Location = new System.Drawing.Point(32, 56);
            this.lblMatiere.Name = "lblMatiere";
            this.lblMatiere.Size = new System.Drawing.Size(48, 13);
            this.lblMatiere.TabIndex = 25;
            this.lblMatiere.Text = "Matière :";
            // 
            // lblMoyenneBrute
            // 
            this.lblMoyenneBrute.AutoSize = true;
            this.lblMoyenneBrute.Location = new System.Drawing.Point(32, 300);
            this.lblMoyenneBrute.Name = "lblMoyenneBrute";
            this.lblMoyenneBrute.Size = new System.Drawing.Size(84, 13);
            this.lblMoyenneBrute.TabIndex = 27;
            this.lblMoyenneBrute.Text = "Moyenne brute :";
            // 
            // txtMoyenneBrute
            // 
            this.txtMoyenneBrute.Enabled = false;
            this.txtMoyenneBrute.Location = new System.Drawing.Point(137, 297);
            this.txtMoyenneBrute.Name = "txtMoyenneBrute";
            this.txtMoyenneBrute.Size = new System.Drawing.Size(100, 20);
            this.txtMoyenneBrute.TabIndex = 28;
            // 
            // lstNotes
            // 
            this.lstNotes.FormattingEnabled = true;
            this.lstNotes.Location = new System.Drawing.Point(137, 80);
            this.lstNotes.Name = "lstNotes";
            this.lstNotes.Size = new System.Drawing.Size(221, 199);
            this.lstNotes.TabIndex = 30;
            // 
            // lblNotes
            // 
            this.lblNotes.AutoSize = true;
            this.lblNotes.Location = new System.Drawing.Point(32, 92);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(41, 13);
            this.lblNotes.TabIndex = 29;
            this.lblNotes.Text = "Notes :";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(137, 397);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(75, 23);
            this.btnEnregistrer.TabIndex = 31;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // txtCommentaire
            // 
            this.txtCommentaire.Location = new System.Drawing.Point(137, 351);
            this.txtCommentaire.Name = "txtCommentaire";
            this.txtCommentaire.Size = new System.Drawing.Size(100, 20);
            this.txtCommentaire.TabIndex = 34;
            // 
            // lblCommentaire
            // 
            this.lblCommentaire.AutoSize = true;
            this.lblCommentaire.Location = new System.Drawing.Point(32, 354);
            this.lblCommentaire.Name = "lblCommentaire";
            this.lblCommentaire.Size = new System.Drawing.Size(74, 13);
            this.lblCommentaire.TabIndex = 33;
            this.lblCommentaire.Text = "Commentaire :";
            // 
            // frmMoyennesParMatiere
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(780, 450);
            this.Controls.Add(this.txtCommentaire);
            this.Controls.Add(this.lblCommentaire);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.lstNotes);
            this.Controls.Add(this.lblNotes);
            this.Controls.Add(this.txtMoyenneBrute);
            this.Controls.Add(this.lblMoyenneBrute);
            this.Controls.Add(this.lstMatieres);
            this.Controls.Add(this.lblMatiere);
            this.Name = "frmMoyennesParMatiere";
            this.Text = "Affichage moyennes par matière";
            this.Load += new System.EventHandler(this.frmMoyennesParMatiere_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox lstMatieres;
        private System.Windows.Forms.Label lblMatiere;
        private System.Windows.Forms.Label lblMoyenneBrute;
        private System.Windows.Forms.TextBox txtMoyenneBrute;
        private System.Windows.Forms.ListBox lstNotes;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox txtCommentaire;
        private System.Windows.Forms.Label lblCommentaire;
    }
}