﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cours1
{
    public partial class frmMoyennesParMatiere : Form
    {
        public frmMoyennesParMatiere()
        {
            InitializeComponent();
        }

        private void frmMoyennesParMatiere_Load(object sender, EventArgs e)
        {
            foreach (Matiere vMatiere in Variables.getMatieres())
            {
                lstMatieres.Items.Add(vMatiere.getIntitule());
            }
        }

        private void lstMatieres_SelectedIndexChanged(object sender, EventArgs e)
        {
            Releve vReleve = new Releve(Variables.getNotes());
            Matiere vMatiere = Variables.getMatieres().ElementAt(lstMatieres.SelectedIndex);
            lstNotes.Items.Clear();
            lstNotes = vReleve.getNotes(vMatiere, lstNotes);
            txtMoyenneBrute.Text = vReleve.moyenneBruteMatiere(vMatiere).ToString(); //vMoyenne.ToString();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {

        }
    }
}
