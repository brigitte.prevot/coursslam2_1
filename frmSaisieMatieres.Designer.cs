﻿namespace Cours1
{
    partial class frmSaisieMatieres
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIntitule = new System.Windows.Forms.Label();
            this.txtIntitule = new System.Windows.Forms.TextBox();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.txtEnseignant = new System.Windows.Forms.TextBox();
            this.lblEnseignant = new System.Windows.Forms.Label();
            this.txtCoefficient = new System.Windows.Forms.TextBox();
            this.lblCoefficient = new System.Windows.Forms.Label();
            this.txtDuree = new System.Windows.Forms.TextBox();
            this.lblDuree = new System.Windows.Forms.Label();
            this.btnEffacer = new System.Windows.Forms.Button();
            this.lblMatieres = new System.Windows.Forms.Label();
            this.lstMatieres = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblIntitule
            // 
            this.lblIntitule.AutoSize = true;
            this.lblIntitule.Location = new System.Drawing.Point(12, 66);
            this.lblIntitule.Name = "lblIntitule";
            this.lblIntitule.Size = new System.Drawing.Size(44, 13);
            this.lblIntitule.TabIndex = 0;
            this.lblIntitule.Text = "Intitulé :";
            // 
            // txtIntitule
            // 
            this.txtIntitule.Location = new System.Drawing.Point(106, 63);
            this.txtIntitule.Name = "txtIntitule";
            this.txtIntitule.Size = new System.Drawing.Size(100, 20);
            this.txtIntitule.TabIndex = 1;
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(59, 301);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(75, 23);
            this.btnEnregistrer.TabIndex = 5;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // txtEnseignant
            // 
            this.txtEnseignant.Location = new System.Drawing.Point(106, 105);
            this.txtEnseignant.Name = "txtEnseignant";
            this.txtEnseignant.Size = new System.Drawing.Size(100, 20);
            this.txtEnseignant.TabIndex = 2;
            // 
            // lblEnseignant
            // 
            this.lblEnseignant.AutoSize = true;
            this.lblEnseignant.Location = new System.Drawing.Point(12, 108);
            this.lblEnseignant.Name = "lblEnseignant";
            this.lblEnseignant.Size = new System.Drawing.Size(91, 13);
            this.lblEnseignant.TabIndex = 3;
            this.lblEnseignant.Text = "Nom Enseignant :";
            // 
            // txtCoefficient
            // 
            this.txtCoefficient.Location = new System.Drawing.Point(106, 147);
            this.txtCoefficient.Name = "txtCoefficient";
            this.txtCoefficient.Size = new System.Drawing.Size(100, 20);
            this.txtCoefficient.TabIndex = 3;
            // 
            // lblCoefficient
            // 
            this.lblCoefficient.AutoSize = true;
            this.lblCoefficient.Location = new System.Drawing.Point(12, 150);
            this.lblCoefficient.Name = "lblCoefficient";
            this.lblCoefficient.Size = new System.Drawing.Size(63, 13);
            this.lblCoefficient.TabIndex = 5;
            this.lblCoefficient.Text = "Coefficient :";
            // 
            // txtDuree
            // 
            this.txtDuree.Location = new System.Drawing.Point(106, 193);
            this.txtDuree.Name = "txtDuree";
            this.txtDuree.Size = new System.Drawing.Size(100, 20);
            this.txtDuree.TabIndex = 4;
            // 
            // lblDuree
            // 
            this.lblDuree.AutoSize = true;
            this.lblDuree.Location = new System.Drawing.Point(12, 196);
            this.lblDuree.Name = "lblDuree";
            this.lblDuree.Size = new System.Drawing.Size(42, 13);
            this.lblDuree.TabIndex = 7;
            this.lblDuree.Text = "Durée :";
            // 
            // btnEffacer
            // 
            this.btnEffacer.Location = new System.Drawing.Point(173, 301);
            this.btnEffacer.Name = "btnEffacer";
            this.btnEffacer.Size = new System.Drawing.Size(75, 23);
            this.btnEffacer.TabIndex = 6;
            this.btnEffacer.Text = "Effacer";
            this.btnEffacer.UseVisualStyleBackColor = true;
            this.btnEffacer.Click += new System.EventHandler(this.btnEffacer_Click);
            // 
            // lblMatieres
            // 
            this.lblMatieres.AutoSize = true;
            this.lblMatieres.Location = new System.Drawing.Point(435, 63);
            this.lblMatieres.Name = "lblMatieres";
            this.lblMatieres.Size = new System.Drawing.Size(53, 13);
            this.lblMatieres.TabIndex = 10;
            this.lblMatieres.Text = "Matières :";
            // 
            // lstMatieres
            // 
            this.lstMatieres.FormattingEnabled = true;
            this.lstMatieres.Location = new System.Drawing.Point(536, 51);
            this.lstMatieres.Name = "lstMatieres";
            this.lstMatieres.Size = new System.Drawing.Size(221, 199);
            this.lstMatieres.TabIndex = 11;
            // 
            // SaisieMatiere
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lstMatieres);
            this.Controls.Add(this.lblMatieres);
            this.Controls.Add(this.btnEffacer);
            this.Controls.Add(this.txtDuree);
            this.Controls.Add(this.lblDuree);
            this.Controls.Add(this.txtCoefficient);
            this.Controls.Add(this.lblCoefficient);
            this.Controls.Add(this.txtEnseignant);
            this.Controls.Add(this.lblEnseignant);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.txtIntitule);
            this.Controls.Add(this.lblIntitule);
            this.Name = "SaisieMatiere";
            this.Text = "Saisie de matières";
            this.Load += new System.EventHandler(this.SaisieMatiere_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIntitule;
        private System.Windows.Forms.TextBox txtIntitule;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox txtEnseignant;
        private System.Windows.Forms.Label lblEnseignant;
        private System.Windows.Forms.TextBox txtCoefficient;
        private System.Windows.Forms.Label lblCoefficient;
        private System.Windows.Forms.TextBox txtDuree;
        private System.Windows.Forms.Label lblDuree;
        private System.Windows.Forms.Button btnEffacer;
        private System.Windows.Forms.Label lblMatieres;
        private System.Windows.Forms.ListBox lstMatieres;
    }
}

