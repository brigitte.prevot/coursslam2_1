﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cours1
{
    public partial class frmSaisieMatieres : Form
    {
        public frmSaisieMatieres()
        {
            InitializeComponent();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            try
            {
                Matiere vMatiere = new Matiere(txtIntitule.Text, txtEnseignant.Text, Convert.ToInt32(txtCoefficient.Text), Convert.ToInt32(txtDuree.Text));
                Variables.getMatieres().Add(vMatiere);
                lstMatieres.Items.Add(txtIntitule.Text + " " + txtEnseignant.Text);
                btnEffacer_Click(sender, e);
            }
            catch (NullReferenceException erreur)
            {
                MessageBox.Show(erreur.ToString());
            }
            catch (Exception erreur)
            {
                MessageBox.Show("Vous devez remplir toutes les zones");
            }
        }

        private void btnEffacer_Click(object sender, EventArgs e)
        {
            txtIntitule.Clear();
            txtEnseignant.Clear();
            txtCoefficient.Clear();
            txtDuree.Clear();
        }

        private void SaisieMatiere_Load(object sender, EventArgs e)
        {
            foreach (Matiere vMatiere in Variables.getMatieres())
            {
                lstMatieres.Items.Add(vMatiere.getIntitule() + " " + vMatiere.getEnseignant());
            }
        }
    }
}
