﻿namespace Cours1
{
    partial class frmSaisieNotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstNotes = new System.Windows.Forms.ListBox();
            this.lblNotes = new System.Windows.Forms.Label();
            this.btnEffacer = new System.Windows.Forms.Button();
            this.txtValeur = new System.Windows.Forms.TextBox();
            this.lblValeur = new System.Windows.Forms.Label();
            this.lblMatiere = new System.Windows.Forms.Label();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.txtIntitule = new System.Windows.Forms.TextBox();
            this.lblIntitule = new System.Windows.Forms.Label();
            this.lstMatieres = new System.Windows.Forms.ComboBox();
            this.txtCoefficient = new System.Windows.Forms.TextBox();
            this.lblCoefficient = new System.Windows.Forms.Label();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstNotes
            // 
            this.lstNotes.FormattingEnabled = true;
            this.lstNotes.Location = new System.Drawing.Point(552, 89);
            this.lstNotes.Name = "lstNotes";
            this.lstNotes.Size = new System.Drawing.Size(221, 199);
            this.lstNotes.TabIndex = 19;
            this.lstNotes.SelectedIndexChanged += new System.EventHandler(this.lstNotes_SelectedIndexChanged);
            // 
            // lblNotes
            // 
            this.lblNotes.AutoSize = true;
            this.lblNotes.Location = new System.Drawing.Point(451, 101);
            this.lblNotes.Name = "lblNotes";
            this.lblNotes.Size = new System.Drawing.Size(41, 13);
            this.lblNotes.TabIndex = 22;
            this.lblNotes.Text = "Notes :";
            // 
            // btnEffacer
            // 
            this.btnEffacer.Location = new System.Drawing.Point(189, 339);
            this.btnEffacer.Name = "btnEffacer";
            this.btnEffacer.Size = new System.Drawing.Size(75, 23);
            this.btnEffacer.TabIndex = 18;
            this.btnEffacer.Text = "Effacer";
            this.btnEffacer.UseVisualStyleBackColor = true;
            this.btnEffacer.Click += new System.EventHandler(this.btnEffacer_Click);
            // 
            // txtValeur
            // 
            this.txtValeur.Location = new System.Drawing.Point(122, 185);
            this.txtValeur.Name = "txtValeur";
            this.txtValeur.Size = new System.Drawing.Size(100, 20);
            this.txtValeur.TabIndex = 15;
            // 
            // lblValeur
            // 
            this.lblValeur.AutoSize = true;
            this.lblValeur.Location = new System.Drawing.Point(28, 188);
            this.lblValeur.Name = "lblValeur";
            this.lblValeur.Size = new System.Drawing.Size(43, 13);
            this.lblValeur.TabIndex = 18;
            this.lblValeur.Text = "Valeur :";
            // 
            // lblMatiere
            // 
            this.lblMatiere.AutoSize = true;
            this.lblMatiere.Location = new System.Drawing.Point(28, 146);
            this.lblMatiere.Name = "lblMatiere";
            this.lblMatiere.Size = new System.Drawing.Size(48, 13);
            this.lblMatiere.TabIndex = 16;
            this.lblMatiere.Text = "Matière :";
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(75, 339);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(75, 23);
            this.btnEnregistrer.TabIndex = 17;
            this.btnEnregistrer.Text = "Enregistrer";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.btnEnregistrer_Click);
            // 
            // txtIntitule
            // 
            this.txtIntitule.Location = new System.Drawing.Point(122, 101);
            this.txtIntitule.Name = "txtIntitule";
            this.txtIntitule.Size = new System.Drawing.Size(100, 20);
            this.txtIntitule.TabIndex = 13;
            // 
            // lblIntitule
            // 
            this.lblIntitule.AutoSize = true;
            this.lblIntitule.Location = new System.Drawing.Point(28, 104);
            this.lblIntitule.Name = "lblIntitule";
            this.lblIntitule.Size = new System.Drawing.Size(44, 13);
            this.lblIntitule.TabIndex = 12;
            this.lblIntitule.Text = "Intitulé :";
            // 
            // lstMatieres
            // 
            this.lstMatieres.FormattingEnabled = true;
            this.lstMatieres.Location = new System.Drawing.Point(122, 146);
            this.lstMatieres.Name = "lstMatieres";
            this.lstMatieres.Size = new System.Drawing.Size(100, 21);
            this.lstMatieres.TabIndex = 14;
            // 
            // txtCoefficient
            // 
            this.txtCoefficient.Location = new System.Drawing.Point(122, 229);
            this.txtCoefficient.Name = "txtCoefficient";
            this.txtCoefficient.Size = new System.Drawing.Size(100, 20);
            this.txtCoefficient.TabIndex = 16;
            // 
            // lblCoefficient
            // 
            this.lblCoefficient.AutoSize = true;
            this.lblCoefficient.Location = new System.Drawing.Point(28, 232);
            this.lblCoefficient.Name = "lblCoefficient";
            this.lblCoefficient.Size = new System.Drawing.Size(57, 13);
            this.lblCoefficient.TabIndex = 26;
            this.lblCoefficient.Text = "Coefficient";
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.Location = new System.Drawing.Point(303, 339);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(75, 23);
            this.btnSupprimer.TabIndex = 20;
            this.btnSupprimer.Text = "Supprimer";
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.btnSupprimer_Click);
            // 
            // frmSaisieNotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnSupprimer);
            this.Controls.Add(this.txtCoefficient);
            this.Controls.Add(this.lblCoefficient);
            this.Controls.Add(this.lstMatieres);
            this.Controls.Add(this.lstNotes);
            this.Controls.Add(this.lblNotes);
            this.Controls.Add(this.btnEffacer);
            this.Controls.Add(this.txtValeur);
            this.Controls.Add(this.lblValeur);
            this.Controls.Add(this.lblMatiere);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.txtIntitule);
            this.Controls.Add(this.lblIntitule);
            this.Name = "frmSaisieNotes";
            this.Text = "Saisie des notes";
            this.Load += new System.EventHandler(this.frmSaisieNotes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstNotes;
        private System.Windows.Forms.Label lblNotes;
        private System.Windows.Forms.Button btnEffacer;
        private System.Windows.Forms.TextBox txtValeur;
        private System.Windows.Forms.Label lblValeur;
        private System.Windows.Forms.Label lblMatiere;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.TextBox txtIntitule;
        private System.Windows.Forms.Label lblIntitule;
        private System.Windows.Forms.ComboBox lstMatieres;
        private System.Windows.Forms.TextBox txtCoefficient;
        private System.Windows.Forms.Label lblCoefficient;
        private System.Windows.Forms.Button btnSupprimer;
    }
}