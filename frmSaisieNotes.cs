﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cours1
{
    public partial class frmSaisieNotes : Form
    {
        Note vNote;

        public frmSaisieNotes()
        {
            InitializeComponent();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            try
            {
                Matiere vMatiere = Variables.getMatieres().ElementAt(lstMatieres.SelectedIndex);
                if (Variables.getNotes().Contains(vNote))
                {
                    vNote = Variables.getNotes().ElementAt(lstNotes.SelectedIndex);
                    vNote.setIntitule(txtIntitule.Text);
                    vNote.setMatiere(vMatiere);
                    vNote.setValeur(Convert.ToDouble(txtValeur.Text));
                    vNote.setCoefficient(Convert.ToInt32(txtCoefficient.Text));
                    afficherNotes();
                }
                else
                {
                    vNote = new Note(txtIntitule.Text, Convert.ToDouble(txtValeur.Text), vMatiere, Convert.ToInt32(txtCoefficient.Text));
                    Variables.getNotes().Add(vNote);
                    lstNotes.Items.Add(vMatiere.getIntitule() + " " + txtIntitule.Text + " " + vNote.getValeur() + " " + vNote.getCoefficient());
                }
                btnEffacer_Click(sender, e);
            }
            catch (NullReferenceException erreur)
            {
                MessageBox.Show(erreur.ToString());
            }
            catch (Exception erreur)
            {
                MessageBox.Show("Vous devez remplir toutes les zones");
            }
        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            try
            {
                Variables.getNotes().RemoveAt(lstNotes.SelectedIndex);
                afficherNotes();
                btnEffacer_Click(sender, e);
            }
            catch (Exception erreur)
            {
                MessageBox.Show("Vous devez sélectionner la note à supprimer");
            }
        }

        private void btnEffacer_Click(object sender, EventArgs e)
        {
            txtIntitule.Clear();
            lstMatieres.SelectedIndex = -1;
            txtValeur.Clear();
            txtCoefficient.Clear();
        }
        private void frmSaisieNotes_Load(object sender, EventArgs e)
        {
            foreach(Matiere vMatiere in Variables.getMatieres())
            {
                lstMatieres.Items.Add(vMatiere.getIntitule());
            }
            afficherNotes();
        }

        private void lstNotes_SelectedIndexChanged(object sender, EventArgs e)
        {
            vNote = Variables.getNotes().ElementAt(lstNotes.SelectedIndex);
            txtIntitule.Text = vNote.getIntitule();
            lstMatieres.SelectedIndex = Variables.getMatieres().IndexOf(vNote.getMatiere());
            txtValeur.Text = vNote.getValeur().ToString();
            txtCoefficient.Text = vNote.getCoefficient().ToString();
        }

        private void afficherNotes()
        {
            lstNotes.Items.Clear();
            foreach (Note vNote in Variables.getNotes())
            {
                lstNotes.Items.Add(vNote.getMatiere().getIntitule() + " " + vNote.getIntitule() + " " + vNote.getValeur() + " " + vNote.getCoefficient());
            }
        }
    }
}
